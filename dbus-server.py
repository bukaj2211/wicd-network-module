__author__ = 'kuba'
__author__ = 'stefan'
# myservice.py
# simple python-dbus service that exposes 1 method called hello()

#import gtk
import dbus
import dbus.service
from dbus.mainloop.glib import DBusGMainLoop
import time


class MyDBUSService(dbus.service.Object):
    def __init__(self):
        self.bus_name = dbus.service.BusName('com.skriware.dbustest', bus=dbus.SessionBus())
        dbus.service.Object.__init__(self, self.bus_name, '/com/skriware/dbustest')
        self.sending = False


    @dbus.service.signal(dbus_interface='com.skriware.dbustest')
    def SendStartScanSignal(self):
        self.sending = True

    def looploop(self):
        while True:
            self.SendStartScanSignal()
            print("JEB")
            time.sleep(2)

if __name__ == '__main__':
    DBusGMainLoop(set_as_default=True)
    myservice = MyDBUSService()
    myservice.looploop()