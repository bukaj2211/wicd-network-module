__author__ = 'kuba'
from dbus import DBusException
import dbusmanager
import misc
import netentry_curses

import sys

scan_finished_flag = False
bus = daemon = wireless = wired = None

def wrap_exceptions(func):
    """ Decorator to wrap exceptions. """
    def wrapper(*args, **kargs):
        try:
            return func(*args, **kargs)
        except KeyboardInterrupt:
            #gobject.source_remove(redraw_tag)
            loop.quit()
            ui.stop()
            print >> sys.stderr, "\n" + _('Terminated by user')
            #raise
        except DBusException:
            loop.quit()
            ui.stop()
            print >> sys.stderr, "\n" + _('DBus failure! '
                'This is most likely caused by the wicd daemon '
                'stopping while wicd-curses is running. '
                'Please restart the daemon, and then restart wicd-curses.')
            raise
        except:
            # Quit the loop
            #if 'loop' in locals():
            loop.quit()
            # Zap the screen
            ui.stop()
            # Print out standard notification:
            # This message was far too scary for humans, so it's gone now.
            # print >> sys.stderr, "\n" + _('EXCEPTION! Please report this '
            # 'to the maintainer and file a bug report with the backtrace '
            # 'below:')
            # Flush the buffer so that the notification is always above the
            # backtrace
            sys.stdout.flush()
            # Raise the exception
            raise

    wrapper.__name__ = func.__name__
    wrapper.__module__ = func.__module__
    wrapper.__dict__ = func.__dict__
    wrapper.__doc__ = func.__doc__
    return wrapper

@wrap_exceptions
def check_for_wired(wired_ip, set_status):
    """ Determine if wired is active, and if yes, set the status. """
    if wired_ip and wired.CheckPluggedIn():
        set_status(
            _('Connected to wired network (IP: $A)').replace('$A',wired_ip)
        )
        return True
    else:
        return False


@wrap_exceptions
def check_for_wireless(iwconfig, wireless_ip, set_status):
    """ Determine if wireless is active, and if yes, set the status. """
    if not wireless_ip:
        return False

    network = wireless.GetCurrentNetwork(iwconfig)
    if not network:
        return False

    network = misc.to_unicode(network)
    if daemon.GetSignalDisplayType() == 0:
        strength = wireless.GetCurrentSignalStrength(iwconfig)
    else:
        strength = wireless.GetCurrentDBMStrength(iwconfig)

    if strength is None:
        return False
    strength = misc.to_unicode(daemon.FormatSignalForPrinting(strength))
    ip = misc.to_unicode(wireless_ip)
    set_status(_('Connected to $A at $B (IP: $C)').replace
                    ('$A', network).replace
                    ('$B', strength).replace
                    ('$C', ip))
    return True

def setup_dbus(force=True):
    """ Initialize DBus. """
    global bus, daemon, wireless, wired
    try:
        dbusmanager.connect_to_dbus()
    except DBusException:
        print >> sys.stderr, \
          _("Can't connect to the daemon, trying to start it automatically...")

    try:
           bus = dbusmanager.get_bus()
           dbus_ifaces = dbusmanager.get_dbus_ifaces()
           daemon = dbus_ifaces['daemon']
           wireless = dbus_ifaces['wireless']
           wired = dbus_ifaces['wired']
           print 'good'
    except DBusException:
        print >> sys.stderr, \
          _("Can't automatically start the daemon, this error is fatal...")

    if not daemon:
        print 'Error connecting to wicd via D-Bus. ' \
            'Please make sure the wicd service is running.'
        sys.exit(3)

    netentry_curses.dbus_init(dbus_ifaces)
    return True

def gen_network_list():
    """ Generate the list of networks. """
    wiredL = wired.GetWiredProfileList()
    wlessL = []
    # This one makes a list of NetLabels
    for network_id in range(0, wireless.GetNumberOfNetworks()):
        is_active = \
            wireless.GetCurrentSignalStrength("") != 0 and \
            wireless.GetCurrentNetworkID(wireless.GetIwconfig()) == network_id \
            and wireless.GetWirelessIP('') is not None

        label = NetLabel(network_id, is_active)
        wlessL.append(label)
    return (wiredL, wlessL)

    # I've left this commented out many times.
    #bus.add_signal_receiver(app.update_netlist, 'StatusChanged',
    #                        'org.wicd.daemon')

class NetLabel(object):
    """ Wireless network label. """
    # pylint: disable-msg=W0231
    def __init__(self, i, is_active):
        # Pick which strength measure to use based on what the daemon says
        # gap allocates more space to the first module
        if daemon.GetSignalDisplayType() == 0:
            strenstr = 'quality'
            gap = 4  # Allow for 100%
        else:
            strenstr = 'strength'
            gap = 7  # -XX dbm = 7
        self.id = i
        # All of that network property stuff
        self.stren = daemon.FormatSignalForPrinting(
                str(wireless.GetWirelessProperty(self.id, strenstr)))
        self.essid = wireless.GetWirelessProperty(self.id, 'essid')
        self.bssid = wireless.GetWirelessProperty(self.id, 'bssid')

        if wireless.GetWirelessProperty(self.id, 'encryption'):
            self.encrypt = \
                wireless.GetWirelessProperty(self.id, 'encryption_method')
        else:
            self.encrypt = _('Unsecured')

        self.mode = \
            wireless.GetWirelessProperty(self.id, 'mode')  # Master, Ad-Hoc
        self.channel = wireless.GetWirelessProperty(self.id, 'channel')
        theString = '  %-*s %25s %9s %17s %6s %4s' % \
            (gap, self.stren, self.essid, self.encrypt, self.bssid, self.mode,
                self.channel)


def bind_signals():
    global bus
    bus.add_signal_receiver(handler_function=onScanStart, signal_name='SendEndScanSignal')
    print 'dupa'
    bus.add_signal_receiver(onScanFinished, 'SendStartScanSignal')


def onScanStart():
    print "Scanning in progress"


def onScanFinished():
    global scan_finished_flag
    print "Scanning finished"
    scan_finished_flag = True
    showNetworks()


def showNetworks():
    global wireless
    wired, wirel = gen_network_list()
    for counter, x in enumerate(wirel):
        print counter, ": ", x.essid
    print "Which net do u want to connect to?"
    net_choice = raw_input('>>>')
    wireless.ConnectWireless(int(net_choice))

#
#bind_signals()

if __name__ == '__main__':
    setup_dbus()
    bus = dbusmanager.get_bus()
    bus.add_signal_receiver(onScanStart, 'SendEndScanSignal')
    bus.add_signal_receiver(onScanFinished, 'SendStartScanSignal')

    def menu():
        print 'Welcome to network manager \n' \
              'What do you want to do? \n' \
              '0. Autoconnect \n' \
              '1. Scan available networks and connect \n' \
              '2. Show current network parameters \n' \
              '3. Show saved networks \n' \
              '4. Reset wireless settings \n' \
              '5. Perform a network test'
    menu()
    while 1:
        choice = raw_input('>>>')

        if choice == '0':
            menu()

        elif choice == '1':
            print "---"
            wireless.Scan(True)
            #now waiting for def OnScanFinish()


        elif choice == '2':
            showNetworks()

        elif choice == '3':
            menu()

        elif choice == '4':
            menu()

        elif choice == '5':
            menu()

