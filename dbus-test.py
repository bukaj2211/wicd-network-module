__author__ = 'kuba'
import dbus
import sys
import gobject



if getattr(dbus, "version", (0, 0, 0)) < (0, 80, 0):
    import dbus.glib
else:
    print 'starting DBusGMainLoop'
    from dbus.mainloop.glib import DBusGMainLoop
    DBusGMainLoop(set_as_default=True)

bus = daemon = wireless = wired = None

class DBusManager(object):
    """ Manages the DBus objects used by wicd. """
    def __init__(self):
        self._bus = dbus.SessionBus()
        self._dbus_ifaces = {}

    def connect_to_dbus(self):
        """ Connects to wicd's dbus interfaces and loads them into a dict. """
        proxy_obj = self._bus.get_object("com.skriware.dbustest", '/com/skriware/dbustest')
        daemon = dbus.Interface(proxy_obj, 'com.skriware.dbustest')


        self._dbus_ifaces = {"daemon" : daemon}

    def get_dbus_ifaces(self):
        """ Returns a dict of dbus interfaces. """
        if not self._dbus_ifaces:
            self.connect_to_dbus()
        return self._dbus_ifaces

    def get_interface(self, iface):
        """ Returns a DBus Interface. """
        if not self._dbus_ifaces:
            self.connect_to_dbus()
        return self._dbus_ifaces[iface]

    def get_bus(self):
        """ Returns the loaded SystemBus. """
        return self._bus

    def set_mainloop(self, loop):
        """ Set DBus main loop. """
        dbus.set_default_main_loop(loop)



if __name__ == '__main__':
    dbusmanager = DBusManager()

    def setup_dbus(force=True):
        """ Initialize DBus. """
        global bus, daemon, wireless, wired
        try:
            dbusmanager.connect_to_dbus()
        except dbus.DBusException:
            print >> sys.stderr, \
              ("Can't connect to the daemon, trying to start it automatically...")

        try:
               bus = dbusmanager.get_bus()
               dbus_ifaces = dbusmanager.get_dbus_ifaces()
               daemon = dbus_ifaces['daemon']
               print 'good'
        except dbus.DBusException:
            print >> sys.stderr, \
              ("Can't automatically start the daemon, this error is fatal...")

        if not daemon:
            print 'Error connecting to wicd via D-Bus. ' \
                'Please make sure the wicd service is running.'
            sys.exit(3)

        return True

    def dbus_handler():
        print "dbus signal SendStartScanSignal received"

    setup_dbus()
    bus = dbusmanager.get_bus()
    bus.add_signal_receiver(dbus_handler, 'SendStartScanSignal', 'com.skriware.dbustest')

    loop = gobject.MainLoop()
    loop.run()
    while 1:
        pass