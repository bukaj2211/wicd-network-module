__author__ = 'kuba'
from twisted.internet import reactor, defer

from txdbus import client, error


def onSignal():
    print 'Got SendStartScanSignal'


@defer.inlineCallbacks
def main():

    try:
        cli   = yield client.connect(reactor, "system")

        daemon  = yield cli.getRemoteObject( 'org.wicd.daemon', '/org/wicd/daemon' )

        wireless = yield cli.getRemoteObject( 'org.wicd.daemon', '/org/wicd/daemon', interfaces='/org/wicd/daemon/wireless' )

        wireless.notifyOnSignal('SendStartScanSignal', onSignal)

        wireless.Scan()
    except error.DBusException, e:
        print 'DBus Error:', e



reactor.callWhenRunning(main)
reactor.run()